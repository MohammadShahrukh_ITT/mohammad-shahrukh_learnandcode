import java.util.Scanner;

class BinaryNode {
    BinaryNode left, right;
    int data;

    public BinaryNode() {
        left = null;
        right = null;
        data = 0;
    }

    public BinaryNode(int number) {
        left = null;
        right = null;
        data = number;
    }

    public int getData() {
        return data;
    }
}

 class BinaryTree {

    private BinaryNode root;

    public void BinaryTree() {
        root = null;
    }

    public void insert(int number) {
        root = insertNode(root, number);
    }

    private BinaryNode insertNode(BinaryNode node, int number) {
        if (node == null) {
            node = new BinaryNode(number);
        } else {
            if (number <= node.getData()) {
                node.left = insertNode(node.left, number);
            } else {
                node.right = insertNode(node.right, number);
            }
        }
        return node;
    }
    public int getMaxDepth() {
        return getNodeDepth(root);
}
    private int getNodeDepth(BinaryNode node) {
        if (node == null) {
            return 0;
        } else {
            int leftNodeHeight = 0;
            int rightNodeHeight = 0;

            if(node.left != null)
            	leftNodeHeight = getNodeDepth(node.left);
            if(node.right != null)
            	rightNodeHeight = getNodeDepth(node.right);
            if (leftNodeHeight > rightNodeHeight) {
                return leftNodeHeight + 1;
            } else {
                return rightNodeHeight + 1;
            }
        }
    }
}

public class MonkWatchingFight {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        BinaryTree bst = new BinaryTree();
         
        int noOfNodes = sc.nextInt();
        for(int i=0; i < noOfNodes; i++) {
            bst.insert(sc.nextInt());
        }
        System.out.println(bst.getMaxDepth());
    }   
}
