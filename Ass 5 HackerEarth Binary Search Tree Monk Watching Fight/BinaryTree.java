package custombinarysearchtree;

class BinaryNode {

    BinaryNode left, right;
    int data;

    public BinaryNode() {
        left = null;
        right = null;
        data = 0;
    }

    public BinaryNode(int number) {
        left = null;
        right = null;
        data = number;
    }

    public void setLeftNode(BinaryNode node) {
        left = node;
    }

    public BinaryNode getLeftNode() {
        return left;
    }

    public void setRightNode(BinaryNode node) {
        right = node;
    }

    public BinaryNode getRightNode() {
        return right;
    }

    public void setData(int number) {
        data = number;
    }

    public int getData() {
        return data;
    }
}

public class BinaryTree {

    private BinaryNode root;

    public void BinaryTree() {
        root = null;
    }

    public void insert(int number) {
        root = insertNode(root, number);
    }

    public BinaryNode insertNode(BinaryNode node, int number) {
        if (node == null) {
            node = new BinaryNode(number);
        } else {
            if (node.getData() <= number) {
                node.left = insertNode(node.left, number);
            } else {
                node.right = insertNode(node.right, number);
            }
        }
        return node;
    }

    public boolean search(int number) {
        return searchNode(root, number);
    }

    public boolean searchNode(BinaryNode node, int number) {
        if (node == null) {
            return false;
        } else {
            if (node.getData() == number) {
                return true;
            } else if (node.getData() < number) {
                searchNode(node.left, number);
            } else if (node.getData() > number) {
                searchNode(node.right, number);
            }
        }
        return false;
    }

    public void inOrder() {
        inOrderTranversel(root);
    }

    public void inOrderTranversel(BinaryNode node) {
        if (node != null) {
            inOrderTranversel(node.getLeftNode());
            System.out.println(node.getData() + " ");
            inOrderTranversel(node.getRightNode());
        }
    }

    public void preOrder() {
        preOrderTransversel(root);
    }

    public void preOrderTransversel(BinaryNode node) {
        if (node != null) {
            System.out.println(node.getData() + " ");
            preOrderTransversel(node.getLeftNode());
            preOrderTransversel(node.getRightNode());
        }
    }

    public void postOrder() {
        postOrderTransversel(root);
    }

    public void postOrderTransversel(BinaryNode node) {
        if (node != null) {
            postOrderTransversel(node.getLeftNode());
            postOrderTransversel(node.getRightNode());
            System.out.println(node.getData() + " ");
        }
    }

    public boolean isEmpty() {
        if (root == null) {
            return true;
        }
        return false;
    }

    public void delete(int number) {
        if (isEmpty()) {
            System.out.println("Tree is Empty");
        } else if (search(number) == false) {
            System.out.println(number + " not found");
        } else {
            deleteNode(root, number);
        }
    }

    public BinaryNode deleteNode(BinaryNode node, int number) {
        BinaryNode p, p2, tempNode;
        if (root.getData() == number) {
            BinaryNode leftNode, rightNode;
            leftNode = root.getLeftNode();
            rightNode = root.getRightNode();
            if (leftNode == null && rightNode == null) {
                return null;
            } else if (leftNode == null) {
                p = rightNode;
                return p;
            } else if (rightNode == null) {
                p = leftNode;
                return p;
            } else {
                p2 = rightNode;
                p = rightNode;
                while (p.getLeftNode() != null) {
                    p = p.getLeftNode();
                }
                p.setLeftNode(leftNode);
                return p2;
            }
        }
        if (number < root.getData()) {
            tempNode = deleteNode(root.getLeftNode(), number);
            root.setLeftNode(tempNode);
        } else {
            tempNode = deleteNode(root.getRightNode(), number);
            root.setRightNode(tempNode);
        }
        return root;
    }

    public int getMaxDepth() {
        return getNodeDepth(root);
    }

    public int getNodeDepth(BinaryNode node) {
        if (node == null) {
            return 0;
        } else {
            int leftNodeHeight = 0;
            int rightNodeHeight = 0;
            if (node.left != null) {
                leftNodeHeight = getNodeDepth(node.left);
            }
            if (node.right != null) {
                rightNodeHeight = getNodeDepth(node.right);
            }
            if (leftNodeHeight < rightNodeHeight) {
                return leftNodeHeight + 1;
            } else {
                return rightNodeHeight + 1;
            }
        }
    }
}
