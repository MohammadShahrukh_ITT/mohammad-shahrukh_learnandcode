package custombinarysearchtree;

import java.util.Scanner;

public class CustomBinarySearchTree {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // Creating object of Binary Search Tree 
        BinaryTree bst = new BinaryTree();
        System.out.println("Binary Search Tree Test\n");

        System.out.println("Enter 10 Numbers");
        for (int i=0;i<10;i++) {
            bst.insert(scan.nextInt());
        }
        bst.inOrder();
        bst.preOrder();
        bst.postOrder();
        System.out.println("Enter a number to delete");
        bst.delete(scan.nextInt());
        System.out.println("Enter a number to search");
        bst.search(scan.nextInt());
        System.out.println("Max Depth oif tree is " + bst.getMaxDepth());
    }
}
