#include <iostream>
using namespace std;

int main() 
{
	int testCase,i=0;
	
    	//Enter number of test cases.
	cin>>testCase;
	int cases=testCase;
	int arr[testCase];

    	while(cases--) 
	{
   	 	int passes,player_id, last_id;
		 //Enter no of passes and first player Id
    		cin>>passes;
		cin>>player_id;
		
    		last_id=player_id;

    		while(passes--)
		{
        		
			char action;
	      		cin>>action; 

               		if(action == 'P')
			{
            			
				int id;
            			cin>>id;
            			last_id = player_id;
            			player_id = id;
        		}
        		else
			{
               			player_id=last_id+player_id;
               			last_id=player_id-last_id;
               			player_id=player_id-last_id;
        		}
   		 }
   		 	
    		arr[i++]=player_id;
    	}
    	for(int k=0;k<testCase;k++)
    	{ cout<<"Player "<<arr[k]<<endl;
		}
    	return 0;
}