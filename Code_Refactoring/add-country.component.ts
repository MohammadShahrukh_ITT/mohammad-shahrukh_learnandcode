import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedDataService } from '../../services/shared-data.service';
import { DemoPipe } from '../../pipes/demo.pipe';
import { QuotedataService } from '../../services/quotedata.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.css']
})
export class AddCountryComponent implements OnInit {
  @Output() closeCountryPopup = new EventEmitter();

  regions: any;
  countries: any;
  selectedCountries: any = [];
  selectedRegions: any = [];
  filteredCountryList: any;
  currentNode: any;
  currentQuoteRef: any;
  existingCountries: any;

  constructor(private _sharedDataService: SharedDataService, private _quotedataService: QuotedataService,
    private _toastr: ToastrService) { }

  ngOnInit() {
    this._quotedataService.getReferenceData().subscribe((data: any) => {
      this.filteredCountryList = this.countries = data.countries;
      this.regions = data.regions;
    });

    this._sharedDataService.addCountryInfo.subscribe((addCountryInf: any) => {
      if (Object.keys(addCountryInf).length !== 0) {
        this.currentNode = addCountryInf.currentNode;
        this.currentQuoteRef = addCountryInf.quoteRef;
        this.existingCountries = [];
        this.selectedCountries = [];
        this.selectedRegions = [];
        if (this.currentNode) {
          for (let region of this.currentQuoteRef.children) {
            for (let country of region.children) {
              this.existingCountries.push(country.data);
            }
          }
        }
      }
    });
  }

  addRemoveCountry(event, country) {
    if (event.target.checked) {
      this.selectedCountries.push(country);
    }
    else {
      if (this.selectedCountries.indexOf(country) > -1) {
        let index = this.selectedCountries.indexOf(country);
        this.selectedCountries.splice(index, 1);
      }
    }
  }

  selectUnselectRegion(event, region) {
    if (event.target.checked) {
      this.selectedRegions.push(region);
      for (let country of this.countries) {
        if (country.regionId === region.id) {
          this.selectedCountries.indexOf(country) > -1 || this.isCountryAlreadyAdded(country) ? null : this.selectedCountries.push(country);
        }
      }
    }
    else {
      if (this.selectedRegions.indexOf(region) > -1) {
        let index = this.selectedRegions.indexOf(region);
        this.selectedRegions.splice(index, 1);
      }
      for (let country of this.countries) {
        if (country.regionId === region.id) {
          let index = this.selectedCountries.indexOf(country);
          index > -1 ? this.selectedCountries.splice(index, 1) : null;
        }
      }
    }
    this.filterCountry();
  }

  addCountry() {
    if (this.selectedCountries.length == 0) {
      this._toastr.error("Please select atleast one country");
    }
    else {
      let addCountryList: any = {};
      addCountryList.countries = this.selectedCountries;
      this._quotedataService.addCountriesToQuote(this.currentQuoteRef.data.id, addCountryList).subscribe(res => {
        this._quotedataService.getQuoteList().subscribe(quoteList => {
          this._sharedDataService.setQuoteList(quoteList);
          this._toastr.success("Countries added successfully");
          this._sharedDataService.updateCurrentNode(this.getNewNodeData());
          this.closePopup();
        });
      });
    }
  }

  filterCountry() {
    if (this.selectedRegions.length === 0) {
      this.filteredCountryList = this.countries;
    }
    else {
      this.filteredCountryList = [];
      for (let region of this.selectedRegions) {
        for (let country of this.countries) {
          if (region.id === country.regionId)
            this.filteredCountryList.push(country);
        }
      }
    }
  }

  isCountryAvailable(country) {
    return this.selectedCountries.indexOf(country) > -1 || this.isCountryAlreadyAdded(country);
  }

  isRegionSelected(region) {
    return this.selectedRegions.indexOf(region) > -1;
  }

  isCountryAlreadyAdded(country) {
    for (let _country of this.existingCountries) {
      if (_country.id === country.id)
        return true;
    }
  }

  closePopup() {
    this.selectedCountries = [];
    this.selectedRegions = [];
    this.existingCountries = [];
    this._sharedDataService.setCurrentNode(this.currentNode);
    this.closeCountryPopup.emit();
  }

  getNewNodeData() {
    let newNodeInfo: any = {
      data: {}
    };

    newNodeInfo.data.quoteId = this.currentQuoteRef.data.id;
    newNodeInfo.data.regionId = this.selectedCountries[0].regionId;
    newNodeInfo.data.id = this.selectedCountries[0].id;
    newNodeInfo.data.isCountry = true;

    return newNodeInfo;
  }
}
