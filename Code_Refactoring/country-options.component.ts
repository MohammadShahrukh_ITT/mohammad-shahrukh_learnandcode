import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedDataService } from '../../services/shared-data.service';
import { Conditional } from '@angular/compiler';
import { Router } from '@angular/router';
import { DemoPipe } from '../../pipes/demo.pipe';

@Component({
  selector: 'app-country-options',
  templateUrl: './country-options.component.html',
  styleUrls: ['./country-options.component.css']
})
export class CountryOptionsComponent implements OnInit {
  countryOptions: any;
  disableRegion: boolean = false;
  disablecountry: boolean = false;
  disableCountryOptions: boolean = false;
  selectedRegions: any;
  filteredCountryList: any;


  constructor(private _sharedDataService: SharedDataService, private _route: Router) {
    this._sharedDataService.countryOptions.subscribe(countryOption => {
      this.countryOptions = countryOption;
      this.selectedRegions = [];
      this.filteredCountryList = this.countryOptions.countries;
    });
  }

  ngOnInit() {
    this._sharedDataService.activeNode.subscribe(node => {
      let activeNode = node;
      if (activeNode === "region") {
        this.disableRegion = true;
        this.disablecountry = false;
        this.disableCountryOptions = false;
      }
      else if (activeNode === "country") {
        this.disableRegion = true;
        this.disablecountry = true;
        this.disableCountryOptions = false;
      }
      else if (activeNode === "device") {
        this.disableRegion = true;
        this.disablecountry = true;
        this.disableCountryOptions = true;
      }
      else {
        this.disableRegion = false;
        this.disablecountry = false;
        this.disableCountryOptions = false;
      }
    });
  }

  submitCountryOptions() {
    this.countryOptions.isChanged = true;
    this._sharedDataService.setCountryOptions(this.countryOptions);
  }

  selectUnselectRegion(event, region) {
    if (event.target.checked) {
      this.selectedRegions.push(region);
      for (let country of this.countryOptions.countries) {
        if (country.regionId === region.id) {
          country.isSelected = true;
        }
      }
    }
    else {
      if (this.selectedRegions.indexOf(region) > -1) {
        let index = this.selectedRegions.indexOf(region);
        this.selectedRegions.splice(index, 1);
      }
      for (let country of this.countryOptions.countries) {
        if (country.regionId == region.id) {
          country.isSelected = false;
        }
      }
    }
    this.filterCountry();
  }

  disabledCountry(country) {
    country.isSelected = true;
    return true;
  }

  filterCountry() {
    if (this.selectedRegions.length === 0) {
      this.filteredCountryList = this.countryOptions.countries
    }
    else {
      this.filteredCountryList = [];
      for (let region of this.selectedRegions) {
        for (let country of this.countryOptions.countries) {
          if (region.id === country.regionId) {    
            this.filteredCountryList.push(country);
          }
        }
      }
    }
  }
}
