import { Component, OnInit, Input } from '@angular/core';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedDataService } from '../../services/shared-data.service';
import { DemoPipe } from '../../pipes/demo.pipe';

@Component({
  selector: 'app-device-options',
  templateUrl: './device-options.component.html',
  styleUrls: ['./device-options.component.css']
})
export class DeviceOptionsComponent implements OnInit {
  deviceOptions: any;
  disableCountry: boolean = false;
  disableDevice: boolean = false;
  enableAssesories: boolean = false;
  disableDeviceOptions: any = false;
  selectedAccessories: any;
  filteredCountryList: any;
  selectedDevices: any;
  constructor(private _sharedDataService: SharedDataService) {
    this._sharedDataService.deviceOptions.subscribe(deviceOption => {
      this.deviceOptions = deviceOption;
      this.deviceOptions ? this.deviceOptions.devices.length === 0 ? this.disableDeviceOptions = true : this.disableDeviceOptions = false : null;
      this.deviceOptions ? this.selectedAccessories = this.getSelectedAccessories(this.deviceOptions.accessories) : null;
      this.selectedAccessories ? this._sharedDataService.setSelectedAccessoriesForConfig(this.selectedAccessories) : null;
      this.filteredCountryList = this.deviceOptions.countries;
      this.selectedDevices = [];
    });
  }

  ngOnInit() {
    this._sharedDataService.activeNode.subscribe(node => {
      let activeNode = node;
      if (activeNode === "country") {
        this.disableCountry = true;
        this.disableDevice = false;
        this.enableAssesories = false;
      }
      else if (activeNode === "device") {
        this.disableCountry = true;
        this.disableDevice = true;
        this.enableAssesories = true;
      }
      else {
        this.disableCountry = false;
        this.disableDevice = false;
        this.enableAssesories = false;
      }
    });
  }

  selectAccessory(event, accessory) {
    if (event.target.checked) {
      this.selectedAccessories.push(accessory);
    }
    else {
      if (this.selectedAccessories.indexOf(accessory) > -1) {
        let index = this.selectedAccessories.indexOf(accessory);
        this.selectedAccessories.splice(index, 1);
      }
    }
    this._sharedDataService.setSelectedAccessoriesForConfig(this.selectedAccessories);
    this.submitDeviceOptions();
  }

  submitDeviceOptions() {
    this.deviceOptions.isChanged = true;
    this._sharedDataService.setDeviceOptions(this.deviceOptions);
  }

  getSelectedAccessories(accessories) {
    let preSelectedAccessories: any = [];
    for (let accessory of accessories) {
      if (accessory.isSelected)
        preSelectedAccessories.push(accessory);
    }
    return preSelectedAccessories;
  }

  selectUnselectDevices(event, device) {
    if (event.target.checked) {
      this.selectedDevices.push(device);
      for (let countryId of device.countryIds) {
        for (let country of this.deviceOptions.countries) {
          if (country.id === countryId) {
            country.isSelected = true;
          }
        }
      }
    }
    else {
      if (this.selectedDevices.indexOf(device) > -1) {
        let index = this.selectedDevices.indexOf(device);
        this.selectedDevices.splice(index, 1);
      }
      for (let countryId of device.countryIds) {
        for (let country of this.deviceOptions.countries) {
          if (country.id === countryId) {
            country.isSelected = false;
          }
        }
      }
    }
    this.filterCountry();
  }

  filterCountry() {
    if (this.selectedDevices.length === 0) {
      this.filteredCountryList = this.deviceOptions.countries;
    }
    else {
      this.filteredCountryList = [];
      for (let device of this.selectedDevices) {
        for (let countryId of device.countryIds) {
          for (let country of this.deviceOptions.countries) {
            if (country.id === countryId) {
              if (!(this.filteredCountryList.indexOf(country) > -1))
                this.filteredCountryList.push(country);
            }
          }
        }
      }
    }
  }

  // selectAllDevices(event) {
  //   if (event.target.checked) {
  //     for (let device of this.deviceOptions.devices) {
  //       device.isSelected = true;
  //       this.selectedDevices.push(device);
  //     }
  //   }
  //   else {
  //     for (let device of this.deviceOptions.devices) {
  //       device.isSelected = false;
  //     }
  //     this.selectedDevices = [];
  //   }
  //   this.filterCountry();
  // }

  selectDeviceCountry(device) {
    for (let device of this.deviceOptions.devices) {
      device.isSelected = true;
    }
    for (let country of this.deviceOptions.countries) {
      country.isSelected = true;
    }
    return true;
  }

}