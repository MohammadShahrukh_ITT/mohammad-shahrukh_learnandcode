import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedDataService } from '../../services/shared-data.service';
import { DemoPipe } from '../../pipes/demo.pipe';
import { QuotedataService } from '../../services/quotedata.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.css']
})
export class AddDeviceComponent implements OnInit {
  @Output() closeDevicePopup = new EventEmitter();

  quoteId: any;
  selectedCountries: any;
  addDeviceData: any = {};
  filterCountries: any;
  currentNode: any;
  currentQuoteRef: any;
  existingDevices: any;
  isNodeviceAvailable:boolean = false;

  constructor(private _sharedDataService: SharedDataService,
    private _quoteDataService: QuotedataService,
    private _toastr: ToastrService) { }

  ngOnInit() {
    this._sharedDataService.addDeviceInfo.subscribe((addDeviceInf: any) => {
      if (Object.keys(addDeviceInf).length !== 0) {
        this.selectedCountries = [];
        this.existingDevices = [];
        this.filterCountries = [];
        this.currentQuoteRef = addDeviceInf.quoteRef;
        this.currentNode = addDeviceInf.currentNode;
        this.quoteId = this.currentQuoteRef.data.id;       
        if (this.currentQuoteRef) {
          for (let region of this.currentQuoteRef.children) {
            for (let country of region.children) {
              if (country.children.length !== 0)
                for (let device of country.children) {
                  this.existingDevices.push(device.data);
                }
            }
          }
        }

        this._quoteDataService.getAddDeviceData(this.quoteId).subscribe(( data:any) => {
          this.addDeviceData = data;
          this.isNodeviceAvailable  = (this.addDeviceData.devices.length === 0 );
        });
      }
    });
  }

  addRemoveCountry(event, country) {
    if (event.target.checked) {
      this.selectedCountries.push(country);
    }
    else {
      if (this.selectedCountries.indexOf(country) > -1) {
        let index = this.selectedCountries.indexOf(country);
        this.selectedCountries.splice(index, 1);
      }
    }
  }

  selectAllCountries(event) {
    if (event.target.checked) {
      this.selectedCountries = [];
      for (let country of this.addDeviceData.countries) {
        this.isDeviceAlreadyAddedInCountry(country) ? null : this.selectedCountries.push(country)
      }
    }
    else {
      this.selectedCountries = [];
    }
  }

  addDevice() {
    let deviceinfo: any = {};
    deviceinfo.quantity = this.addDeviceData.quantity;
    deviceinfo.monthlyMonoPages = this.addDeviceData.monthlyMonoPages;
    deviceinfo.monthlyColorPages = this.addDeviceData.monthlyColorPages;
    deviceinfo.percentageProfColor = this.addDeviceData.percentageProfColor;
    deviceinfo.purchaseMethodId = this.addDeviceData.purchaseMethodId;
    deviceinfo.countries = this.selectedCountries;
    deviceinfo.deviceSKU = this.addDeviceData.deviceSKU;
    deviceinfo.devices = null;

    if (this.selectedCountries.length === 0) {
      this._toastr.warning("Please select atleast one country");
    }
    else {
      this._quoteDataService.addDevicesToQuote(this.quoteId, deviceinfo).subscribe(res => {
        this._quoteDataService.getQuoteList().subscribe(quoteList => {
          this._sharedDataService.setQuoteList(quoteList);
          this._toastr.success("Device added successfully");
          this._sharedDataService.updateCurrentNode(this.getNewNodeData());
          this.closePopup();
        });
      });
    }
  }

  isCountryChecked(country) {
    return this.selectedCountries.indexOf(country) > -1 || this.isDeviceAlreadyAddedInCountry(country);
  }

  closePopup() {
    this.closeDevicePopup.emit();
  }

  filterCountriesData() {
    this.selectedCountries = [];
    let device;
    for (let _device of this.addDeviceData.devices) {
      if (this.addDeviceData.deviceSKU === _device.sku) {
        device = _device;
        break;
      }
    }
    if (device) {
      let filetredCountries = [];
      for (let countryId of device.countryIds) {
        for (let country of this.addDeviceData.countries) {
          if (countryId === country.id) {
            filetredCountries.push(country);
          }
        }
      }
      this.filterCountries = filetredCountries;
    }
  }

  isDeviceAlreadyAddedInCountry(country) {
    for (let device of this.existingDevices) {
      if (device.name === this.addDeviceData.deviceSKU)
        if (country.id === device.countryId)
          return true;
    }
  }

  getNewNodeData() {
    let newNodeData: any = {
      data: {}
    };
    newNodeData.data.quoteId = this.quoteId;
    newNodeData.data.regionId = this.selectedCountries[0].regionId;
    newNodeData.data.countryId = this.selectedCountries[0].id;
    newNodeData.data.sku = this.addDeviceData.deviceSKU;
    newNodeData.data.isDevice = true;

    return newNodeData;
  }
}
