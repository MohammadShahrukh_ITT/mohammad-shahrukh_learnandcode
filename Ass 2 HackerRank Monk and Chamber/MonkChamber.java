

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

class Spider {

    private int index;
    private int power;

    Spider(int index, int power) {
        this.index = index;
        this.power = power;
    }

    public int getIndex() {
        return index;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}

public class MonkChamber {

    public static final int MIN_INPUT_SIZE = 1;
    public static final int MAX_INPUT_SIZE = 316;
    public int queueSize;
    public static int noOfSpidersToDequeue;
    public String powersOfSpider;
    public static void main(String args[]) throws IOException {
        
        MonkChamber monkChamber = new MonkChamber();
		
	
         Queue<Spider> spiderList = monkChamber.readInputs();
        monkChamber.GameStart(noOfSpidersToDequeue, spiderList);
    }

    public Queue<Spider> readInputs()throws IOException {
       	//reading inputs
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String queueSizeAndIterations[] = reader.readLine().split(" ");
        int[] tempArr = new int[2];
        int temp = 0;
        for (String inputs : queueSizeAndIterations) {
            tempArr[temp++] = Integer.parseInt(inputs);
        }
        MonkChamber monkChamber = new MonkChamber();
        queueSize = monkChamber.queueSizeValidation(tempArr[0]);
        noOfSpidersToDequeue = monkChamber.inputSizeValidation(tempArr[1], tempArr[0]);

        powersOfSpider = reader.readLine();
        String[] power = powersOfSpider.split(" ");
		
	//assigned powers to respective index of spider class
        Queue<Spider> spiderList = new LinkedList<Spider>();
        int index = 1;
        for (String p : power) {
            Spider s = new Spider(index, Integer.parseInt(p));
            spiderList.add(s);
            index++;
        }
        return spiderList;
    }

    public int spidersPickedFromQueue(Queue<Spider> spiderList, int iterations) {
        int noOfPickedSpiders;
        if (spiderList.size() < iterations) {
            noOfPickedSpiders = spiderList.size();
        } else {
            noOfPickedSpiders = iterations;
        }
        return noOfPickedSpiders;
    }

    public int findMaxIndex(ArrayList<Spider> spiders) {
        int max = 0;
        int maxIndex = 0;
        for (int index = 0; index < spiders.size(); index++) {
            if (max < spiders.get(index).getPower()) {
                max = spiders.get(index).getPower();
                maxIndex = index;
            }
        }
        return maxIndex;
    }
	
    public ArrayList<Spider> decreaseSpidersPower(ArrayList<Spider> spiders) {
        for (Spider sp : spiders) {
            if (sp.getPower() != 0) {
                sp.setPower((int) sp.getPower() - 1);
            }
        }
        return spiders;
    }

    public void GameStart(int dequedSpiders, Queue<Spider> spiderList) {
        for (int iteration = 0; iteration < dequedSpiders; iteration++) {
            
            ArrayList<Spider> spiders = new ArrayList<>();
            int noOfElements = spidersPickedFromQueue(spiderList, dequedSpiders);

            for (int index = 0; index < noOfElements; index++) {
                spiders.add(spiderList.remove());
            }
            int maxIndex = findMaxIndex(spiders);
            System.out.print(spiders.get(maxIndex).getIndex() + " ");
            spiders.remove(maxIndex);
            spiders = decreaseSpidersPower(spiders);
            spiderList.addAll(spiders);
        }
    }

    public int queueSizeValidation(int queuesize) {
        if (queuesize < MIN_INPUT_SIZE || queuesize > MAX_INPUT_SIZE) {
            throw new IllegalArgumentException("Invalid queusize");
        }
        return queuesize;
    }

    public int inputSizeValidation(int inputsize, int queuesize) {
        if (inputsize < queuesize || inputsize > (queuesize * queuesize)) {
            throw new IllegalArgumentException("Invalid inputsize");
        }
        return inputsize;
    }
   
       
 
}
