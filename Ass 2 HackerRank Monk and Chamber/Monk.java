

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;  

class Spider {
	int index;
	int power;
	Spider(int index,int power)
	{
		this.index = index;
		this.power = power;
	}
	public int getIndex() { 
		return index; 
    } 
	public int getpower() { 
        return power; 
    }
        public void setPower(int power)
        { this.power=power;
        }
}

public class MonkChamber {  
		
 public static void main(String args[]) {
	int queueSize;
	int iterations;
	String powersOfSpider;
	try{
	    BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
	String input[] = sc.readLine().split(" ");
        int[] queit = new int[2];
        int temp=0;
        for(String inputs : input)
            queit[temp++] = Integer.parseInt(inputs);
	queueSize = queit[0];
	iterations = queit[1];
        
	powersOfSpider = sc.readLine();
	//System.out.println(queueSize+" " + iterations + " " + powersOfSpider);
	
	String[] power = powersOfSpider.split(" ");
 
   Queue<Spider> spiderList=new LinkedList<Spider>();  
   int index=1;
	for(String p:power){ 
		Spider s = new Spider(index,Integer.parseInt(p)); 
		spiderList.add(s);  
		index++;
	}
    GameStart(iterations,spiderList);
       
     }
        catch(Exception e)
                {
                    e.printStackTrace();
                }
 }

private static int elementsToBePicked(Queue<Spider> spiderList, int iterations) {
    int noOfElements;
    if(spiderList.size()<iterations) 
    noOfElements=spiderList.size();
    else
        noOfElements=iterations;
    return noOfElements;
}

private static int findMaxIndex(ArrayList<Spider> spiders) {
        int max=0;
        int maxIndex=0;
    for(int i = 0;i<spiders.size(); i++) {
            //System.out.println(spiders.get(i).getpower());
            if(max<spiders.get(i).getpower())
            {
                    max=spiders.get(i).getpower();
                    maxIndex=i;
            }
    }
    return maxIndex;
}

private static ArrayList<Spider> decreaseSpidersPower(ArrayList<Spider> spiders) {
     for (Spider sp : spiders) {
    // System.out.println("Powers " + spider.getIndex()+" " + spider.getpower());
    if(sp.getpower()!=0)
    sp.setPower((int)sp.getpower()-1);
 }
     return spiders;
}

    private static void GameStart(int iterations, Queue<Spider> spiderList) {
         for(int j=0;j<iterations;j++)
        {
           ArrayList<Spider> spiders=new ArrayList<>();
           int noOfElements=elementsToBePicked(spiderList,iterations);

           for(int i = 0; i < noOfElements; i++) {
                   spiders.add(spiderList.remove());
                   //spiderList.remove(0);
           } 
           int maxIndex = findMaxIndex(spiders);
           System.out.print(spiders.get(maxIndex).getIndex()+" ");
           spiders.remove(maxIndex);
           spiders = decreaseSpidersPower(spiders);
           spiderList.addAll(spiders);
       }
    }
} 

