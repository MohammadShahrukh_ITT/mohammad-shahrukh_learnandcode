/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unittestcases;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mohammad.shahrukh
 */
public class BinaryTreeTest {
    
    @Test
    public void TestTreeHeightWhenTreeIsEmpty() {
        System.out.println("Testing, Empty Binary Search Tree");
        BinaryTree bst = new BinaryTree();
        BinaryNode node = null;
        assertEquals(0, bst.getNodeDepth(node));
    }

    @Test
    public void TestInsertNodeNotNull() {
        System.out.println("Testing, Tree is not empty after inserting a Node");
        BinaryTree bst = new BinaryTree();
        BinaryNode root = null;
        assertNotNull(bst.insertNode(root, 10));
    }
    @Test
    public void TestNewNodeChilds() {
        System.out.println("Testing, Left Child and Right Child is null of a new created Node");
        BinaryTree bst = new BinaryTree();
        BinaryNode root = null;
        assertEquals(14, bst.insertNode(root, 14).getData());
        assertNull(bst.insertNode(root, 14).getLeftNode());
        assertNull(bst.insertNode(root, 14).getLeftNode());
    }
    @Test
    public void treeHeightWithAllLeftChild() {
        System.out.println("Testing tree height, when all nodes are left Childs");
        BinaryTree bst = new BinaryTree();
        BinaryNode root = null;
        root = bst.insertNode(root, 1);
        root = bst.insertNode(root, 2);
        root = bst.insertNode(root, 3);
        root= bst.insertNode(root, 4);
        assertEquals(4, bst.getNodeDepth(root));
    }
    @Test
    public void treeHeightWithAllRightChild() {
        System.out.println("Testing tree height, while all nodes are right child of tree");
         BinaryTree bst = new BinaryTree();
        BinaryNode root = null;
        root = bst.insertNode(root, 9);
        root = bst.insertNode(root, 7);
        root = bst.insertNode(root, 5);
        root= bst.insertNode(root, 1);
        assertEquals(4, bst.getNodeDepth(root));
    }
    @Test
    public void treeHeightWithLeftRightChild() {
        System.out.println("Testing tree hieght,when tree have both left child and right child");
         BinaryTree bst = new BinaryTree();
        BinaryNode root = null;
        root = bst.insertNode(root, 23);
        root = bst.insertNode(root, 7);
        root = bst.insertNode(root, 15);
        root= bst.insertNode(root, 12);
        root= bst.insertNode(root, 18);
        root= bst.insertNode(root, 73);
        root= bst.insertNode(root, 4);
        assertEquals(4, bst.getNodeDepth(root));
    }
    
}
