/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unittestcases;

/**
 *
 * @author mohammad.shahrukh
 */

import java.util.Scanner;


class BinaryNode {

    BinaryNode left, right;
    int data;

    public BinaryNode() {
        left = null;
        right = null;
        data = 0;
    }

    public BinaryNode(int number) {
        left = null;
        right = null;
        data = number;
    }

    public void setLeftNode(BinaryNode node) {
        left = node;
    }

    public BinaryNode getLeftNode() {
        return left;
    }

    public void setRightNode(BinaryNode node) {
        right = node;
    }

    public BinaryNode getRightNode() {
        return right;
    }

    public void setData(int number) {
        data = number;
    }

    public int getData() {
        return data;
    }
}

 class BinaryTree {

    private BinaryNode root;

    public void BinaryTree() {
        root = null;
    }

    public void insert(int number) {
        root = insertNode(root, number);
    }

    public BinaryNode insertNode(BinaryNode node, int number) {
        if (node == null) {
            node = new BinaryNode(number);
        } else {
            if (node.getData() < number) {
                node.left = insertNode(node.left, number);
            } else {
                node.right = insertNode(node.right, number);
            }
        }
        return node;
    }
    public int getMaxDepth() {
   return getNodeDepth(root);
}
  public void inOrder() {
        inOrderTranversel(root);
    }

    public void inOrderTranversel(BinaryNode node) {
        if (node != null) {
            inOrderTranversel(node.getLeftNode());
            System.out.println(node.getData() + " ");
            inOrderTranversel(node.getRightNode());
        }
    }
    public int getNodeDepth(BinaryNode node) {
        if (node == null) {
            return 0;
        } else {
            int leftNodeHeight = 0;
            int rightNodeHeight=0;
            if(node.left!=null)
             leftNodeHeight = getNodeDepth(node.left);
            if(node.right!=null)
             rightNodeHeight = getNodeDepth(node.right);
            if (leftNodeHeight > rightNodeHeight) {
                return leftNodeHeight + 1;
            } else {
                return rightNodeHeight + 1;
            }
        }
    }
}

public class MonkWatchingFight {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
         BinaryTree bst = new BinaryTree();
         
        int noOfNodes = sc.nextInt();
        for(int i=0; i<noOfNodes; i++) {
            bst.insert(sc.nextInt());
        }
        bst.inOrder();
        System.out.println(bst.getMaxDepth());
    }
    
}

