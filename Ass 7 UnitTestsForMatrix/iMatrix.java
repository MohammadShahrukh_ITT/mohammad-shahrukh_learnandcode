
package matrixtest;

public interface iMatrix {

    public static final int ROWS_IN_MATRIX = 3;
    public static final int COLS_IN_MATRIX = 3;
    Integer[][] inputMatrix = new Integer[ROWS_IN_MATRIX][COLS_IN_MATRIX];

    public Integer[][] getInputMatrixFromUser();

    public void displayResults();

    public void calculateSumOfAlternateIndexElements(Integer[][] inputMatrix);
    
    public void getDataAtSpecificLocation();
}
