package matrixtest;

import org.junit.Assert;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;

public class MatrixTest {

    @Test
    public void testInputElemetsOfMatrix() {
        // Arrange
        Matrix userMatrix = new Matrix();
        Integer array[][] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

        // Act
        Integer[][] expectedMatrix = userMatrix.getInputMatrixFromUser();

        // Assert
        assertArrayEquals(array, expectedMatrix);
    }

    @Test
    public void testSumOfAlternateIndexElements() {
        // Arrange
        Matrix userMatrix = new Matrix();
        Integer array[][] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        userMatrix.getInputMatrixFromUser();
        
        // Act
        userMatrix.calculateSumOfAlternateIndexElements(array);

        // Assert
        Assert.assertEquals(25, userMatrix.sumOfEvenIndexElements);
        Assert.assertEquals(20, userMatrix.sumOfOddIndexElements);
    }
  
  @Test
  public void testGetDataAtSpecificLocation() {
	    //Arrange
		Matrix userMatrix = new Matrix();
        Integer array[][] = {{1, 2, 3}, {4, 5, 6}};
		int elementAtLocation0x0 = userMatrix.getDataAtSpecificLocation(0,0);
		int elementAtLocation0x1 = userMatrix.getDataAtSpecificLocation(0,1);
		int elementAtLocation0x2 = userMatrix.getDataAtSpecificLocation(0,2);
		int elementAtLocation0x0 = userMatrix.getDataAtSpecificLocation(1,0);
		int elementAtLocation1x1 = userMatrix.getDataAtSpecificLocation(1,1);
		int elementAtLocation1x2 = userMatrix.getDataAtSpecificLocation(1,2);
		
		//Assert
		assertEquals(1,elementAtLocation0x0);
		assertEquals(2,elementAtLocation0x1);
		assertEquals(3,elementAtLocation0x2);
		assertEquals(4,elementAtLocation1x0);
		assertEquals(5,elementAtLocation1x1);
		assertEquals(6,elementAtLocation1x2);
		
  }
}
