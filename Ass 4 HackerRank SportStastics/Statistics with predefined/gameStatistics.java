import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

/**
 *
 * @author mohammad.shahrukh
 */
public class GameStatistics {

    /**
     * @param args the command line arguments
     */
       static int maxVoteCount=0;
       static String mostPopularSportName="";
       static  Scanner sc = new Scanner(System.in);
       static Hashtable<String, Integer> statistics = new Hashtable<String,Integer>();
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
     
     
       int noOfEntries = sc.nextInt();
       findMostPopularSport(noOfEntries);
        displayResults();
    }

    private static void findMostPopularSport(int noOfEntries) {
         ArrayList voterList = new ArrayList();
         for(int entries=0; entries<noOfEntries; entries++) {
            String voter = sc.next();
            String sport = sc.next();
             if(!voterList.contains(voter)) {
                 voterList.add(voter);
                 if(statistics.containsKey(sport)) {
                      int votes = statistics.get(sport)+1;
                    statistics.put(sport, votes);
                    if(votes >= maxVoteCount) {
                        maxVoteCount = votes;
                        mostPopularSportName = sport;
                    }
                }
                else {
                    statistics.put(sport, 1);
                }
                 
             }
       }
    }

    private static void displayResults() {
        System.out.println(mostPopularSportName);
         System.out.println(statistics.get("football")==null ? 0 :statistics.get("football").intValue()) ;
    }
    
}