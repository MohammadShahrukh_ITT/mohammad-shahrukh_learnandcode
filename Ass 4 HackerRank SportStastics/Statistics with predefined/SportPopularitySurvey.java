import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author mohammad.shahrukh
 */
public class SportPopularitySurvey {

       static Scanner sc = new Scanner(System.in);
       static Hashtable<String, Integer> sportPopularityTable = new Hashtable<String, Integer>();
       static SportPopularitySurvey survey = new SportPopularitySurvey();
    public static void main(String [] args) {
        
        int noOfPersons = sc.nextInt();
        survey.CollectVotes(noOfPersons);
        survey.displayResults();
    }

     void CollectVotes(int noOfPersons) {
        ArrayList voterList = new ArrayList();
        String VoterName="";
        String SportName="";
        for(int vote=0; vote<noOfPersons; vote++) {
            VoterName = sc.next();
            SportName = sc.next();
            survey.addToSportsPopularityTable(voterList,VoterName,SportName);
        }
    }

    private void addToSportsPopularityTable(ArrayList voterList, String VoterName, String SportName) {
      if (!voterList.contains(VoterName)) {
               voterList.add(VoterName);
              if (sportPopularityTable.containsKey(SportName)) {
                   int votes = sportPopularityTable.get(SportName) + 1;
                   sportPopularityTable.put(SportName, votes);
               } else {
                  sportPopularityTable.put(SportName, 1);
               }
    }
    }

    private void displayResults() {
        Set<String> keys = sportPopularityTable.keySet();
        int maxVotes=0;
        String mostPopularSport="";
        for(String key: keys){
            if(maxVotes < sportPopularityTable.get(key).intValue()){
                maxVotes = sportPopularityTable.get(key).intValue();
                mostPopularSport = key;
         }
        }
        System.out.println(mostPopularSport);
       System.out.println(sportPopularityTable.get("football") == null ? 0 : sportPopularityTable.get("football").intValue());
    }
}