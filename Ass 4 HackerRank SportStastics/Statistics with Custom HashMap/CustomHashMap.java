/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sportspopularitysurveywithcustomhashmap;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author mohammad.shahrukh
 */
class HashNode<K, V> {

    K key;
    V Value;
    int hashCode;

    public HashNode(K key, V value) {
        this.key = key;
        this.Value = value;
        hashCode = key.hashCode();
    }
}

public class CustomHashMap<K, V> {

    K key;
    V value;
    ArrayList<LinkedList<HashNode<K, V>>> hashBucket = new ArrayList<LinkedList<HashNode<K, V>>>();
    int bucketSize = 8;  //default bucket size

    public CustomHashMap() {
        //initializing bucket wuth null entries
        for (int i = 0; i < bucketSize; i++) {
            hashBucket.add(null);
        }
    }

    public void put(K key, V value) {
        int currentHashCode = key.hashCode();
        int currentIndexOfBucket = currentHashCode % bucketSize;
        if (hashBucket.get(currentIndexOfBucket) != null) {
            LinkedList<HashNode<K, V>> currentLinkedList = hashBucket.get(currentIndexOfBucket);
            currentLinkedList.add(new HashNode<K, V>(key, value));
        } else {
            LinkedList<HashNode<K, V>> temp = new LinkedList<HashNode<K, V>>();
            temp.add(new HashNode<K, V>(key, value));
            hashBucket.set(currentIndexOfBucket, temp);
        }
    }

    public V get(K key) {
        int currentHashCode = key.hashCode();
        int currentIndexOfBucket = currentHashCode % bucketSize;
        if (hashBucket.get(currentIndexOfBucket) != null) {
            LinkedList<HashNode<K, V>> temp = hashBucket.get(currentIndexOfBucket);
            for (HashNode<K, V> hashnode : temp) {
                if (hashnode.key.equals(key)) {
                    return hashnode.Value;
                }
            }
        } else {
            return null;
        }
        return null;
    }

    public boolean contains(K key) {
        int currentHashCode = key.hashCode();
        int currentIndexOfBucket = currentHashCode % bucketSize;
        if (hashBucket.get(currentIndexOfBucket) != null) {
            LinkedList<HashNode<K, V>> temp = hashBucket.get(currentIndexOfBucket);
            for (HashNode<K, V> hashnode : temp) {
                if (hashnode.key.equals(key)) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    public ArrayList<K> keyset() {
        ArrayList<K> keyset = new ArrayList<K>();
        for (LinkedList<HashNode<K, V>> bucket : hashBucket) {
            if (bucket == null) {
                continue;
            }
            for (HashNode<K, V> currentNode : bucket) {
                keyset.add(currentNode.key);
            }
        }
        return keyset;
    }

    public void printHashMap() {
        for (LinkedList<HashNode<K, V>> bucket : hashBucket) {
            if (bucket == null) {
                continue;
            }
            for (HashNode<K, V> currentNode : bucket) {
                System.out.println("<" + currentNode.key + "," + currentNode.Value + ">\n");
            }
        }
    }
}
