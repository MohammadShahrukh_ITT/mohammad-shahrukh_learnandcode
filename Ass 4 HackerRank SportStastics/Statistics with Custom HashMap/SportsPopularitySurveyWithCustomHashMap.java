/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sportspopularitysurveywithcustomhashmap;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author mohammad.shahrukh
 */
public class SportsPopularitySurveyWithCustomHashMap {

    static Scanner sc = new Scanner(System.in);
    static CustomHashMap<String, Integer> sportPopularityTable = new CustomHashMap<String, Integer>();

    public static void main(String[] args) {

        SportsPopularitySurveyWithCustomHashMap survey = new SportsPopularitySurveyWithCustomHashMap();
        int noOfPersons = sc.nextInt();
        survey.CollectVotes(noOfPersons);
        sportPopularityTable.printHashMap();
        survey.displayResults();
    }

    public void CollectVotes(int noOfPersons) {
        ArrayList voterList = new ArrayList();
        String VoterName = "";
        String SportName = "";
        for (int vote = 0; vote < noOfPersons; vote++) {
            VoterName = sc.next();
            SportName = sc.next();
            addToSportsPopularityTable(voterList, VoterName, SportName);
        }
    }

    public void addToSportsPopularityTable(ArrayList voterList, String VoterName, String SportName) {
        if (!voterList.contains(VoterName)) {
            voterList.add(VoterName);
            if (sportPopularityTable.contains(SportName)) {
                int votes = sportPopularityTable.get(SportName) + 1;
                sportPopularityTable.put(SportName, votes);
            } else {
                sportPopularityTable.put(SportName, 1);
            }
        }
    }

    private void displayResults() {
        ArrayList<String> keys = sportPopularityTable.keyset();
        int maxVotes = 0;
        String mostPopularSport = "";
        for (String key : keys) {
            if (maxVotes < sportPopularityTable.get(key).intValue()) {
                maxVotes = sportPopularityTable.get(key).intValue();
                mostPopularSport = key;
            }
        }
        System.out.println(mostPopularSport);
        System.out.println(sportPopularityTable.get("football") == null ? 0 : sportPopularityTable.get("football").intValue());
    }
}
