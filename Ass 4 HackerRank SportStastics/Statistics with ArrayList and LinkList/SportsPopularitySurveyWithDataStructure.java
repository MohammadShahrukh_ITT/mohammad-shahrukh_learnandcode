/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sportspopularitysurveywithdatastructure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * @author mohammad.shahrukh
 */
public class SportsPopularitySurveyWithDataStructure {

    /**
     * @param args the command line arguments
     */
    static Scanner sc = new Scanner(System.in);
    static ArrayList<SurveyHashMap> surveyList = new ArrayList<SurveyHashMap>();
    static ArrayList<SportsHashMap> sportsList = new ArrayList<SportsHashMap>();
    static SportsPopularitySurveyWithDataStructure survey = new SportsPopularitySurveyWithDataStructure();
    public static void main(String[] args) {
        // TODO code application logic here
        int noOfPersons = sc.nextInt();
        survey.CollectVotes(noOfPersons);
    }

    private void CollectVotes(int noOfPersons) {
        String VoterName="";
        String SportName="";
        for(int vote=0; vote<noOfPersons; vote++) {
            VoterName = sc.next();
            SportName = sc.next();
            boolean isSurvayTaken = checkSurvay(VoterName,surveyList);
			if(!isSurvayTaken)
			{
				SurveyHashMap survey = new SurveyHashMap();
				survey.setPersonName(VoterName);
				survey.setSportsName(SportName);
				surveyList.add(survey);
				addToSportsPopularityTable(sportsList,SportName);
			}
        }
                SportsHashMap popularSport = findMaxPopularSports(surveyList);
		printFootballAndMaxSports(sportsList,popularSport);
    }

    private boolean checkSurvay(String VoterName, ArrayList<SurveyHashMap> surveyList) {
        for(int i =0;i<surveyList.size();i++)
		{
			if(surveyList.get(i).getPersonName().equals(VoterName))
			{
				System.out.println("already taken");
				return true;
			}
		}
		return false;
    }

    private void addToSportsPopularityTable(ArrayList<SportsHashMap> sportsList, String SportName) {
       int flag =0;
		for(int i =0;i<sportsList.size();i++)
		{
			if(sportsList.get(i).getSportsName().equals(SportName))
			{
				sportsList.get(i).setSportsPopulirity(sportsList.get(i).getSportsPopulirity()+1);
				flag =1;
				break;
			}
		}
		if(flag == 0)
		{
		SportsHashMap sports = new SportsHashMap();
		sports.setSportsName(SportName);
		sports.setSportsPopulirity(1);
		sportsList.add(sports);
		}
    }

    private SportsHashMap findMaxPopularSports(ArrayList<SurveyHashMap> surveyList) {
       SportsHashMap maxPopularSports = new SportsHashMap();
		maxPopularSports.setSportsPopulirity(0);
		for(int i =0;i<sportsList.size();i++)
		{
			if(sportsList.get(i).getSportsPopulirity()>maxPopularSports.getSportsPopulirity())
			{
				maxPopularSports = sportsList.get(i);
			}
		}
		return maxPopularSports;
    }

    private void printFootballAndMaxSports(ArrayList<SportsHashMap> sportsList, SportsHashMap popularSport) {
       int football = 0;
		for(int i=0;i<sportsList.size();i++)
		{
			if(sportsList.get(i).getSportsName().equalsIgnoreCase("football"))
			{
				football = sportsList.get(i).getSportsPopulirity();
				break;
			}
			
		}
			System.out.println("football "+football);
			System.out.println("popular sports "+popularSport.getSportsName());
    }

   
    
}
