import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class RemoveFriendFromFacebook {

    static int totalFriends, noOfFriendsToDelete;
    static BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
    LinkedList<Integer> friendsList = new LinkedList<>();

    public static void main(String[] args) throws IOException {
        RemoveFriendFromFacebook fb = new RemoveFriendFromFacebook();
        int nooftestcases = Integer.parseInt(bufferReader.readLine());
        
        for (int testcase = 0; testcase < nooftestcases; testcase++) {
            fb.getFriendsInput(); //got total no of friends and no of friends criestie decided to delete
            fb.startRemovingFriends(totalFriends, noOfFriendsToDelete);
        }
    }

    private void startRemovingFriends(int totalFriends, int noOfFriendsToDelete) throws IOException {
        String[] popularityArrayOfFriends = bufferReader.readLine().split(" ");
        int currentValueOfDeletedFriends = 0;
        for (int currentfriendIndex = 0; currentfriendIndex < totalFriends; currentfriendIndex++) {
            int currentFriendPopularity = Integer.parseInt(popularityArrayOfFriends[currentfriendIndex]);
            if (currentValueOfDeletedFriends < totalFriends) {
                while (!friendsList.isEmpty() && friendsList.getLast() < currentFriendPopularity && currentValueOfDeletedFriends < noOfFriendsToDelete) {
                    friendsList.removeLast();
                    currentValueOfDeletedFriends++;
                }
            }
            friendsList.addLast(currentFriendPopularity);
        }
        finalFriendListDisplay();
    }

    private void finalFriendListDisplay() {
        while (!friendsList.isEmpty()) {
            System.out.print(friendsList.removeFirst() + " ");
        }
        System.out.println();
    }
		
    private void getFriendsInput() throws IOException {
        String[] friendsInput = bufferReader.readLine().split(" ");
        totalFriends = Integer.parseInt(friendsInput[0]);
        noOfFriendsToDelete = Integer.parseInt(friendsInput[1]);
    }
}
